#!/bin/sh

echo "Syncing chezmoi"
current_dir=pwd
chezmoi update
chezmoi git status
echo "---------------------------------------------------------

Syncing scripts"
cd ~/scripts
git pull
git status
echo "---------------------------------------------------------

Syncing vimwiki"
cd ~/vimwiki
git pull
git status
cd "$currentdir"
