#!/bin/sh

function endless_space() {
  while true; do
    cols=$(tput cols)
    printf "%${cols}s\n" | tr ' ' ' '
    sleep 0.05
  done
}

endless_space | lolcat -i
