# Scripts

These are a bunch of scripts I use on my system

| Script            | Function                                                        |
|-------------------|-----------------------------------------------------------------|
| random-package.sh | Shows a random core repository package and a random aur package |
| shutdown.sh       | Tells me to charge mouse, update packages, then shutdown        |
| sync.sh           | Sync this repo, dotfiles repo and vimwiki repo                  |
| syu.sh            | Uppdate core, aur and flatpak packages                          |
